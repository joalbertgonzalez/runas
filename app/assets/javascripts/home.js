$(document).on('turbolinks:load', () => {
  // referencia al form para buscar empleados
  const $formFindEmployee = $('#find_employee');
  const $notFount = $('#not-fount');
  const $back = $('#back');
   
  $formFindEmployee.on('submit', event => {
    // para prevenir el comportamiento por defecto
    event.preventDefault();
    
    // obtenemos el valor del input #code
    const code = $(event.target).find('#code').val();

    if (code) {
      location.href = `/home/find_employee/${code}`;
      // $.get(`/home/find_employee/${code}`, (data) => {
      //   location.href = `/home/find_employee/${code}`;
      // });
    }
  });

  $notFount.on('click', () => {
    location.href = '/';
  });

  $back.on('click', () => {
    location.href = '/';
  });
});
