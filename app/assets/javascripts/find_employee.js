$(document).on('turbolinks:load', () => {
  // referencia al form para boton entry
  const $btnIn = $('#in');
  // referencia al form para boton departure
  const $btnOut = $('#out');

  // funcion para registrar entrada/salida
  const registryTime = event => {
    const obj = {
      employee: {
        registries_attributes: [
          {
            date_time: new Date(),
            category: event.target.id
          }
        ]
      }
    }

    const code = $(event.target).data('code');

    if (code)
      $.post(`/home/registry_time/${code}`, obj);
  }
  
  $btnIn.on('click', registryTime);
  $btnOut.on('click', registryTime);
});
