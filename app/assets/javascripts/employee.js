$(document).on('turbolinks:load', () => {
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false, // Close upon selecting a date,
    container: undefined, // ex. 'body' will append picker to body
  });

  const $entry = $('#begin');
  const $end = $('#end');

  // Use the picker object directly.
  const beginPicker = $entry.pickadate('picker');
  const endPicker = $end.pickadate('picker');

  try {
    // Using a string along with the parsing format (defaults to `format` option).
    beginPicker.set('select', new Date(), { format: 'yyyy-mm-dd' });
    endPicker.set('select', new Date(), { format: 'yyyy-mm-dd' });
  } catch(e) {}
});
