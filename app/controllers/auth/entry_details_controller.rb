module Auth
  class EntryDetailsController < ApplicationController
    before_action :set_entry_detail, only: [:show, :edit, :update, :destroy]

    # GET /entry_details
    # GET /entry_details.json
    def index
      @entry_details = EntryDetail.all
    end

    # GET /entry_details/1
    # GET /entry_details/1.json
    def show
    end

    # GET /entry_details/new
    def new
      @entry_detail = EntryDetail.new
    end

    # GET /entry_details/1/edit
    def edit
    end

    # POST /entry_details
    # POST /entry_details.json
    def create
      @entry_detail = EntryDetail.new(entry_detail_params)

      respond_to do |format|
        if @entry_detail.save
          format.html { redirect_to @entry_detail, notice: 'Entry detail was successfully created.' }
          format.json { render :show, status: :created, location: @entry_detail }
        else
          format.html { render :new }
          format.json { render json: @entry_detail.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /entry_details/1
    # PATCH/PUT /entry_details/1.json
    def update
      respond_to do |format|
        if @entry_detail.update(entry_detail_params)
          format.html { redirect_to @entry_detail, notice: 'Entry detail was successfully updated.' }
          format.json { render :show, status: :ok, location: @entry_detail }
        else
          format.html { render :edit }
          format.json { render json: @entry_detail.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /entry_details/1
    # DELETE /entry_details/1.json
    def destroy
      @entry_detail.destroy
      respond_to do |format|
        format.html { redirect_to entry_details_url, notice: 'Entry detail was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_entry_detail
      @entry_detail = EntryDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entry_detail_params
      params.require(:entry_detail).permit(:registry_id, :minute_delay, :minute_before)
    end
  end
end
