module Auth
  class RegistriesController < ApplicationController
    before_action :set_registry, only: %i[show edit update destroy]

    # GET /registries
    # GET /registries.json
    def index
      @registries = Registry.all
    end

    # GET /registries/1
    # GET /registries/1.json
    def show; end

    # GET /registries/new
    def new
      @registry = Registry.new
    end

    # GET /registries/1/edit
    def edit; end

    # POST /registries
    # POST /registries.json
    def create
      @registry = Registry.new(registry_params)

      respond_to do |format|
        if @registry.save
          format.html do
            redirect_to [:auth, @registry],
                        notice: 'Registry was successfully created.'
          end
          format.json do
            render :show, status: :created,
                          location: [:auth, @registry]
          end
        else
          format.html { render :new }
          format.json do
            render json: @registry.errors,
                   status: :unprocessable_entity
          end
        end
      end
    end

    # PATCH/PUT /registries/1
    # PATCH/PUT /registries/1.json
    def update
      respond_to do |format|
        if @registry.update(registry_params)
          format.html { redirect_to [:auth, @registry], notice: 'Registry was successfully updated.' }
          format.json { render :show, status: :ok, location: [:auth, @registry] }
        else
          format.html { render :edit }
          format.json { render json: @registry.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /registries/1
    # DELETE /registries/1.json
    def destroy
      @registry.destroy
      respond_to do |format|
        format.html { redirect_to auth_registries_url, notice: 'Registry was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    protected

    def set_layout
      'application'
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_registry
      @registry = Registry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def registry_params
      params.require(:registry).permit(
        :employee_id,
        :date_time,
        :type,
        :ip
      )
    end
  end
end
