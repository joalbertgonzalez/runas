module Auth
  class AdminsController < ApplicationController
    # before_action :valid_admin, :authenticate_user!
    before_action :set_admin, only: %i[show edit update destroy]

    # GET /admins
    # GET /admins.json
    def index
      @admins = Admin.all
    end

    # GET /admins/1
    # GET /admins/1.json
    def show; end

    # GET /admins/new
    def new
      @admin = Admin.new
    end

    # GET /admins/1/edit
    def edit; end

    # POST /admins
    # POST /admins.json
    def create
      @admin = Admin.new(admin_params)

      respond_to do |format|
        if @admin.save
          format.html do
            redirect_to [:auth, @admin],
                        notice: 'Admin was successfully created.'
          end
          format.json do
            render :show, status: :created, location: [:auth, @admin]
          end
        else
          format.html { render :new }
          format.json do
            render json: @admin.errors, status: :unprocessable_entity
          end
        end
      end
    end

    # PATCH/PUT /admins/1
    # PATCH/PUT /admins/1.json
    def update
      respond_to do |format|
        if @admin.update(admin_params)
          format.html do
            redirect_to [:auth, @admin],
                        notice: 'Admin was successfully updated.'
          end
          format.json { render :show, status: :ok, location: [:auth, @admin] }
        else
          format.html { render :edit }
          format.json do
            render json: @admin.errors, status: :unprocessable_entity
          end
        end
      end
    end

    # DELETE /admins/1
    # DELETE /admins/1.json
    def destroy
      @admin.destroy
      respond_to do |format|
        format.html do
          redirect_to auth_admins_url,
                      notice: 'Admin was successfully destroyed.'
        end
        format.json { head :no_content }
      end
    end

    protected

    def set_layout
      'application'
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_admin
      @admin = Admin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the
    # white list through.
    def admin_params
      params.require(:admin).permit(
        :name,
        :active
      )
    end
  end
end
