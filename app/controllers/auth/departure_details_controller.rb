module Auth
  class DepartureDetailsController < ApplicationController
    before_action :set_departure_detail, only: [:show, :edit, :update, :destroy]

    # GET /departure_details
    # GET /departure_details.json
    def index
      @departure_details = DepartureDetail.all
    end

    # GET /departure_details/1
    # GET /departure_details/1.json
    def show
    end

    # GET /departure_details/new
    def new
      @departure_detail = DepartureDetail.new
    end

    # GET /departure_details/1/edit
    def edit
    end

    # POST /departure_details
    # POST /departure_details.json
    def create
      @departure_detail = DepartureDetail.new(departure_detail_params)

      respond_to do |format|
        if @departure_detail.save
          format.html { redirect_to @departure_detail, notice: 'Departure detail was successfully created.' }
          format.json { render :show, status: :created, location: @departure_detail }
        else
          format.html { render :new }
          format.json { render json: @departure_detail.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /departure_details/1
    # PATCH/PUT /departure_details/1.json
    def update
      respond_to do |format|
        if @departure_detail.update(departure_detail_params)
          format.html { redirect_to @departure_detail, notice: 'Departure detail was successfully updated.' }
          format.json { render :show, status: :ok, location: @departure_detail }
        else
          format.html { render :edit }
          format.json { render json: @departure_detail.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /departure_details/1
    # DELETE /departure_details/1.json
    def destroy
      @departure_detail.destroy
      respond_to do |format|
        format.html { redirect_to departure_details_url, notice: 'Departure detail was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_departure_detail
      @departure_detail = DepartureDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def departure_detail_params
      params.require(:departure_detail).permit(:registry_id, :minute_after_work, :minute_before_work)
    end
  end
end
