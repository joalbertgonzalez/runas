module Worker
  class EmployeesController < ApplicationController
    # skip_before_action :verify_authenticity_token
    before_action :set_employee, only: %i[show edit update records]

    # GET /employees/1
    # GET /employees/1.json
    def show; end

    # GET /employees/1/edit
    def edit; end

    # PATCH/PUT /employees/1
    # PATCH/PUT /employees/1.json
    def update
      respond_to do |format|
        if @employee.update(employee_params)
          format.html { redirect_to [:worker, @employee], notice: 'Employee was successfully updated.' }
          format.json { render :show, status: :ok, location: [:worker, @employee] }
        else
          format.html { render :edit }
          format.json { render json: @employee.errors, status: :unprocessable_entity }
        end
      end
    end

    def records
      @registries = []

      @employee.registries.each do |registry|
        @registries << registry.decorate
      end
    end

    protected

    def set_layout
      'application'
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id]).decorate
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(
        :name,
        :last_name,
        :photo_url,
        :dni,
        :civil_status,
        :gender
      )
    end
  end
end
