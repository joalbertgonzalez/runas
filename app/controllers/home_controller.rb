class HomeController < ApplicationController
  before_action :set_employee, only: %i[find_employee registry_time]

  def unregistered; end

  def find_employee; end

  def registry_time
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to unauthenticated_root_path, notice: 'Employee was successfully updated.' }
        format.json { render json: @employee, status: :ok, location: @employee }
      else
        format.html { redirect_to unauthenticated_root_path, notice: 'Employee was not successfully updated.' }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_employee
    @employee = Employee.find_by_code(params[:code]).decorate if Employee.find_by_code(params[:code])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def employee_params
    params.require(:employee).permit(
      registries_attributes: %i[date_time category]
    )
  end
end
