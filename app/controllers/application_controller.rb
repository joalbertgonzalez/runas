class ApplicationController < ActionController::Base

  layout :set_layout

  def after_sign_in_path_for(_resource)
    if current_user.employee?
      [:worker, current_user.profile]
    elsif current_user.admin?
      [:auth, current_user.profile]
    end
  end

  def valid_admin
    return unless user_signed_in?
    destroy_session_user unless current_user.admin?
  end

  def valid_employee
    return unless user_signed_in?
    destroy_session_user unless current_user.employee?
  end

  protected

  def set_layout
    'landing'
  end

  private

  def destroy_session_user
    sign_out(current_user)
    redirect_to unauthenticated_root_path,
                alert: 'You do not have permission to access'
  end
end
