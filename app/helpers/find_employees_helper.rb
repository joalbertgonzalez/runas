module FindEmployeesHelper
  def avatar_name(employee)
    employee.photo_url.present? ? employee.photo_url : 'user-default.png'
  end
end
