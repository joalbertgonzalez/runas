class RegistryDecorator < Draper::Decorator
  delegate_all

  # time debe estar en sec
  def format_hhmmss(time)
    Time.at(time * 60).utc.strftime('%H:%M:%S')
  end

  def evalue_minute_delay
    format_hhmmss(registry.entry_detail ? registry.entry_detail.minute_delay : 0)
  end

  def evalue_minute_before
    format_hhmmss(registry.entry_detail ? registry.entry_detail.minute_before : 0)
  end

  def evalue_minute_after_work
    format_hhmmss(registry.departure_detail ? registry.departure_detail.minute_after_work : 0)
  end

  def evalue_minute_before_work
    format_hhmmss(registry.departure_detail ? registry.departure_detail.minute_before_work : 0)
  end

  def evalue_worker_minutes
    registry.departure_detail ? registry.departure_detail.worker_hours : 0
  end

  def delay?
    'class=delay' if registry.entry_detail.try(:minute_delay).try(:positive?)
  end

  def before_work?
    'class=delay' if registry.departure_detail.try(:minute_before_work).try(:positive?)
  end
end
