class EmployeeDecorator < Draper::Decorator
  delegate_all

  # time debe estar en sec
  def format_hhmmss(time)
    Time.at(time * 60).utc.strftime('%H:%M:%S')
  end

  # si no ha marcado en el dia
  def entry?
    employee.registries.today.present? ? 'disabled' : ''
  end

  # si ya marco entrada, o ya tiene al menos 2 registros entry/departure
  def departure?
    employee.registries.today.present? && employee.registries.today.size == 1 ? '' : 'disabled'
  end

  def delay_hours
    format_hhmmss(employee.minutes_delay)
  end

  def before_work_hours
    format_hhmmss(employee.minutes_before_work)
  end

  def work_hours
    format_hhmmss(employee.worker_hours)
  end
end
