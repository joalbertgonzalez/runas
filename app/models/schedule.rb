class Schedule < ApplicationRecord
  has_many :employees
  has_many :working_days
end
