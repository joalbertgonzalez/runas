# == Schema Information
#
# Table name: working_days
#
#  id             :integer          not null, primary key
#  schedule_id    :integer
#  day            :integer          not null
#  entry_time     :time
#  departure_time :time
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_working_days_on_schedule_id  (schedule_id)
#
# Foreign Keys
#
#  fk_rails_...  (schedule_id => schedules.id)
#

class WorkingDay < ApplicationRecord
  belongs_to :schedule

  enum day: %i[sunday monday tuesday wednesday thursday friday saturday]

  def self.day_week(current_day)
    find_by_day(current_day)
  end
end
