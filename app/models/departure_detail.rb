# == Schema Information
#
# Table name: departure_details
#
#  id                 :integer          not null, primary key
#  registry_id        :integer
#  minute_after_work  :integer          default(0), not null
#  minute_before_work :integer          default(0), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  worker_hours       :integer          default(0), not null
#
# Indexes
#
#  index_departure_details_on_registry_id  (registry_id)
#
# Foreign Keys
#
#  fk_rails_...  (registry_id => registries.id)
#

class DepartureDetail < ApplicationRecord
  belongs_to :registry
end
