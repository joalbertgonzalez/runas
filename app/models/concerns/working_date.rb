module Concerns
  module WorkingDate
    extend ActiveSupport::Concern

    included do
      def time_parse(time)
        Time.zone.parse(time.to_s)
      end

      def cal_sec(in_hour, out_hour)
        in_hour.strftime('%k:%M').to_time - out_hour.strftime('%k:%M').to_time
      end
    end
  end
end
