# == Schema Information
#
# Table name: employees
#
#  id           :integer          not null, primary key
#  name         :string
#  last_name    :string
#  active       :boolean          default(TRUE)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  schedule_id  :integer
#  photo_url    :string
#  code         :integer          not null
#  departament  :string
#  gender       :integer          default("male")
#  dni          :integer
#  civil_status :integer          default("single")
#
# Indexes
#
#  index_employees_on_schedule_id  (schedule_id)
#
# Foreign Keys
#
#  fk_rails_...  (schedule_id => schedules.id)
#

class Employee < ApplicationRecord
  include Concerns::WorkingDate

  FIRST_DAY = 1

  has_one :user, as: :profile
  belongs_to :schedule
  has_many :registries

  has_many :working_days, through: :schedule

  accepts_nested_attributes_for :registries, reject_if: :reject_registries

  enum gender: %i[male female]
  enum civil_status: %i[single wedded widowed divorced]

  # si :date_time o :category no existen, se rechaza
  def reject_registries(attributes)
    attributes[:date_time].blank? || attributes[:category].blank?
  end

  def current_month_first_day
    t = Time.now
    Time.new(t.year, t.month, FIRST_DAY)
  end

  def current_month_last_day
    t = Time.now
    Time.new(t.year, t.month, Time.days_in_month(t.month))
  end

  def records_entry
    registries.date_range_entry(time_parse(current_month_first_day),
                                time_parse(current_month_last_day))
  end

  def minutes_delay
    sum_min = 0
    records_entry.each do |registry|
      sum_min += registry.entry_detail.minute_delay
    end
    sum_min
  end

  def records_departure
    registries.date_range_departure(current_month_first_day,
                                    current_month_last_day)
  end

  def worker_hours
    sum_min = 0
    records_departure.each do |registry|
      sum_min += registry.departure_detail.worker_hours
    end
    sum_min
  end

  def minutes_before_work
    sum_min = 0
    records_departure.each do |registry|
      sum_min += registry.departure_detail.minute_before_work
    end
    sum_min
  end
end
