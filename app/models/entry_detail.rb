# == Schema Information
#
# Table name: entry_details
#
#  id            :integer          not null, primary key
#  registry_id   :integer
#  minute_delay  :integer          default(0), not null
#  minute_before :integer          default(0), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_entry_details_on_registry_id  (registry_id)
#
# Foreign Keys
#
#  fk_rails_...  (registry_id => registries.id)
#

class EntryDetail < ApplicationRecord
  belongs_to :registry
end
