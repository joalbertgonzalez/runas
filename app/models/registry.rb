# == Schema Information
#
# Table name: registries
#
#  id          :integer          not null, primary key
#  employee_id :integer
#  date_time   :datetime         not null
#  category    :integer
#  ip          :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_registries_on_employee_id  (employee_id)
#
# Foreign Keys
#
#  fk_rails_...  (employee_id => employees.id)
#

class Registry < ApplicationRecord
  include Concerns::WorkingDate

  SIXTY_SEC = 60

  has_one :entry_detail
  has_one :departure_detail

  belongs_to :employee, inverse_of: :registries

  before_save :calculate_minutes

  enum category: %i[in out]
  # registry.in! > asigna :in a category
  # registry.in? > true, verifica category es :in
  # registry.category > "in", retorna un string de category
  # Registry.in > consulta todos los registros con category :in
  # enum status: { active: 0, archived: 1 } se puede establecer un objeto
  # Registry.categories > return un objeto json

  # scope :today, ->(date) { where('date_time >= ?', date) }
  scope :today, -> { where('date_time >= ?', Date.today) }
  scope :create_today, -> { where('created_at >= ?', Time.zone.now.beginning_of_day) }
  scope :date_range_departure, ->(first_date, last_date) { where(category: :out, date_time: first_date..last_date) }
  scope :date_range_entry, ->(first_date, last_date) { where(category: :in, date_time: first_date..last_date) }

  private

  def new_entry(entry_time)
    # normalizando los tiempos y aplicando formato 24h
    sec = cal_sec(entry_time, date_time)
    entry_detail = EntryDetail.new

    if sec.negative?
      entry_detail.minute_delay = sec.abs / SIXTY_SEC
      entry_detail.minute_before = 0
    else
      entry_detail.minute_delay = 0
      entry_detail.minute_before = sec.abs / SIXTY_SEC
    end
    self.entry_detail = entry_detail
  end

  def new_departure(departure_time)
    # normalizando los tiempos y aplicando formato 24h
    sec = cal_sec(departure_time, date_time)
    departure_detail = DepartureDetail.new

    if sec.negative?
      departure_detail.minute_after_work = sec.abs / SIXTY_SEC
      departure_detail.minute_before_work = 0
    else
      departure_detail.minute_after_work = 0
      departure_detail.minute_before_work = sec.abs / SIXTY_SEC
    end

    daily_hours(departure_detail)

    self.departure_detail = departure_detail
  end

  def diff_date(date_in, date_out)
    time_parse(date_in) - time_parse(date_out)
  end

  def daily_hours(departure_detail)
    departure_detail.worker_hours = diff_date(@records.first.date_time, date_time).abs / SIXTY_SEC
  end

  def value_entry_departure
    working_day = employee.working_days.day_week(Time.now.wday)
    @entry_time = time_parse(working_day.entry_time)
    @departure_time = time_parse(working_day.departure_time)
  end

  def calculate_minutes
    value_entry_departure
    @records = employee.registries.create_today
    @records.blank? ? new_entry(@entry_time) : new_departure(@departure_time)
  end
end
