json.extract! working_day, :id, :schedule_id, :day, :entry_time, :departure_time, :created_at, :updated_at
json.url working_day_url(working_day, format: :json)
