json.extract! entry_detail, :id, :registry_id, :minute_delay, :minute_before, :created_at, :updated_at
json.url entry_detail_url(entry_detail, format: :json)
