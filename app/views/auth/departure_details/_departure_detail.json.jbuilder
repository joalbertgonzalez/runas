json.extract! departure_detail, :id, :registry_id, :minute_after_work, :minute_before_work, :created_at, :updated_at
json.url departure_detail_url(departure_detail, format: :json)
