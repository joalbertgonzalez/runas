json.extract! registry, :id, :employee_id, :date_time, :type, :ip, :created_at, :updated_at
json.url registry_url(registry, format: :json)
