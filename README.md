# Prueba Runa

## SETUP

* Ruby version `2.4.1`
* `/config/initializers/time.rb`
```rb
Rails.application.config.time_zone = 'Mexico City'
```
* `/config/initializers/generator.rb`
```rb
Rails.application.config.generators do |generate|
  generate.helper false
  generate.javascript_engine false
  generate.request_specs false
  generate.routing_specs false
  generate.stylesheets false
  generate.test_framework :minitest, spec: true, fixture: true
  generate.view_specs false
end
```

## Trello

> [Tareas Rails Runa](https://trello.com/b/t0c9HK7A/runa-prueba-rails)

## Heroku

> [Web Rails Runa](https://test-runa.herokuapp.com/)

## Styles Runa

* Primary: `#7a6ff0`
* Dark: `#352c60`
* Secondary: `#f5a623`

## Screenshots

![Home](./assets/images/home.png "Home")

![Home mobile](./assets/images/home_mobile.png "Home mobile")

![Find employee](./assets/images/find_employee.png "Find employee")

![Profile employee](./assets/images/profile_employee.png "Profile employee")

![Profile employee mobile](./assets/images/profile_employee_mobile.png "Profile employee mobile")

![Records employee](./assets/images/records_employee.png "Records employee")

![Records employee mobile](./assets/images/records_employee_mobile.png "Records employee mobile")

![ER](./assets/images/ER.png "ER")
