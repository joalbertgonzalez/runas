crumb :root do
  link 'Home', auth_admin_path(current_user.profile)
end

crumb :schedules do
  link 'Schedules', auth_schedules_path
end

crumb :schedule do |schedule|
  link schedule.id, auth_schedule_path(schedule)
  parent :admins
end
