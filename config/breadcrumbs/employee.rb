crumb :root do
  link 'Home', auth_admin_path(current_user.profile)
end

crumb :employees do
  link 'Employees', auth_employees_path
end

crumb :employee do |employee|
  link employee.id, auth_employee_path(employee)
  parent :employees
end
