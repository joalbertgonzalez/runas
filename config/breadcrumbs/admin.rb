crumb :root do
  link 'Home', auth_admin_path(current_user.profile)
end

crumb :admins do
  link 'Admins', auth_admins_path
end

crumb :admin do |admin|
  link admin.id, auth_admin_path(admin)
  parent :admins
end
