crumb :root do
  link 'Home', auth_admin_path(current_user.profile)
end

crumb :working_days do
  link 'Working Days', auth_working_days_path
end

crumb :working_day do |working_day|
  link working_day.id, auth_working_day_path(working_day)
  parent :working_days
end
