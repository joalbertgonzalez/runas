crumb :root do
  link 'Home', auth_admin_path(current_user.profile)
end

crumb :registries do
  link 'Registries', auth_registries_path
end

crumb :registry do |registry|
  link registry.id, auth_registry_path(registry)
  parent :registries
end
