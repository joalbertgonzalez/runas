# == Route Map
#
#                    Prefix Verb   URI Pattern                                                                              Controller#Action
#          new_user_session GET    /users/sign_in(.:format)                                                                 devise/sessions#new
#              user_session POST   /users/sign_in(.:format)                                                                 devise/sessions#create
#      destroy_user_session DELETE /users/sign_out(.:format)                                                                devise/sessions#destroy
#         new_user_password GET    /users/password/new(.:format)                                                            devise/passwords#new
#        edit_user_password GET    /users/password/edit(.:format)                                                           devise/passwords#edit
#             user_password PATCH  /users/password(.:format)                                                                devise/passwords#update
#                           PUT    /users/password(.:format)                                                                devise/passwords#update
#                           POST   /users/password(.:format)                                                                devise/passwords#create
#  cancel_user_registration GET    /users/cancel(.:format)                                                                  devise/registrations#cancel
#     new_user_registration GET    /users/sign_up(.:format)                                                                 devise/registrations#new
#    edit_user_registration GET    /users/edit(.:format)                                                                    devise/registrations#edit
#         user_registration PATCH  /users(.:format)                                                                         devise/registrations#update
#                           PUT    /users(.:format)                                                                         devise/registrations#update
#                           DELETE /users(.:format)                                                                         devise/registrations#destroy
#                           POST   /users(.:format)                                                                         devise/registrations#create
#      unauthenticated_root GET    /                                                                                        home#unregistered
#               auth_admins GET    /auth/admins(.:format)                                                                   auth/admins#index
#                           POST   /auth/admins(.:format)                                                                   auth/admins#create
#            new_auth_admin GET    /auth/admins/new(.:format)                                                               auth/admins#new
#           edit_auth_admin GET    /auth/admins/:id/edit(.:format)                                                          auth/admins#edit
#                auth_admin GET    /auth/admins/:id(.:format)                                                               auth/admins#show
#                           PATCH  /auth/admins/:id(.:format)                                                               auth/admins#update
#                           PUT    /auth/admins/:id(.:format)                                                               auth/admins#update
#                           DELETE /auth/admins/:id(.:format)                                                               auth/admins#destroy
#            auth_employees GET    /auth/employees(.:format)                                                                auth/employees#index
#                           POST   /auth/employees(.:format)                                                                auth/employees#create
#         new_auth_employee GET    /auth/employees/new(.:format)                                                            auth/employees#new
#        edit_auth_employee GET    /auth/employees/:id/edit(.:format)                                                       auth/employees#edit
#             auth_employee GET    /auth/employees/:id(.:format)                                                            auth/employees#show
#                           PATCH  /auth/employees/:id(.:format)                                                            auth/employees#update
#                           PUT    /auth/employees/:id(.:format)                                                            auth/employees#update
#                           DELETE /auth/employees/:id(.:format)                                                            auth/employees#destroy
#           auth_registries GET    /auth/registries(.:format)                                                               auth/registries#index
#                           POST   /auth/registries(.:format)                                                               auth/registries#create
#         new_auth_registry GET    /auth/registries/new(.:format)                                                           auth/registries#new
#        edit_auth_registry GET    /auth/registries/:id/edit(.:format)                                                      auth/registries#edit
#             auth_registry GET    /auth/registries/:id(.:format)                                                           auth/registries#show
#                           PATCH  /auth/registries/:id(.:format)                                                           auth/registries#update
#                           PUT    /auth/registries/:id(.:format)                                                           auth/registries#update
#                           DELETE /auth/registries/:id(.:format)                                                           auth/registries#destroy
#         auth_working_days GET    /auth/working_days(.:format)                                                             auth/working_days#index
#                           POST   /auth/working_days(.:format)                                                             auth/working_days#create
#      new_auth_working_day GET    /auth/working_days/new(.:format)                                                         auth/working_days#new
#     edit_auth_working_day GET    /auth/working_days/:id/edit(.:format)                                                    auth/working_days#edit
#          auth_working_day GET    /auth/working_days/:id(.:format)                                                         auth/working_days#show
#                           PATCH  /auth/working_days/:id(.:format)                                                         auth/working_days#update
#                           PUT    /auth/working_days/:id(.:format)                                                         auth/working_days#update
#                           DELETE /auth/working_days/:id(.:format)                                                         auth/working_days#destroy
#            auth_schedules GET    /auth/schedules(.:format)                                                                auth/schedules#index
#                           POST   /auth/schedules(.:format)                                                                auth/schedules#create
#         new_auth_schedule GET    /auth/schedules/new(.:format)                                                            auth/schedules#new
#        edit_auth_schedule GET    /auth/schedules/:id/edit(.:format)                                                       auth/schedules#edit
#             auth_schedule GET    /auth/schedules/:id(.:format)                                                            auth/schedules#show
#                           PATCH  /auth/schedules/:id(.:format)                                                            auth/schedules#update
#                           PUT    /auth/schedules/:id(.:format)                                                            auth/schedules#update
#                           DELETE /auth/schedules/:id(.:format)                                                            auth/schedules#destroy
#                 employees GET    /employees(.:format)                                                                     employees#index
#                           POST   /employees(.:format)                                                                     employees#create
#              new_employee GET    /employees/new(.:format)                                                                 employees#new
#             edit_employee GET    /employees/:id/edit(.:format)                                                            employees#edit
#                  employee GET    /employees/:id(.:format)                                                                 employees#show
#                           PATCH  /employees/:id(.:format)                                                                 employees#update
#                           PUT    /employees/:id(.:format)                                                                 employees#update
#                           DELETE /employees/:id(.:format)                                                                 employees#destroy
#        rails_service_blob GET    /rails/active_storage/blobs/:signed_id/*filename(.:format)                               active_storage/blobs#show
# rails_blob_representation GET    /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations#show
#        rails_disk_service GET    /rails/active_storage/disk/:encoded_key/*filename(.:format)                              active_storage/disk#show
# update_rails_disk_service PUT    /rails/active_storage/disk/:encoded_token(.:format)                                      active_storage/disk#update
#      rails_direct_uploads POST   /rails/active_storage/direct_uploads(.:format)                                           active_storage/direct_uploads#create
# 

Rails.application.routes.draw do
  devise_for :users

  devise_scope :user do
    # authenticated :user do
    #   root 'home#unregistered', as: :authenticated_root
    # end

    unauthenticated do
      # root 'devise/sessions#new', as: :unauthenticated_root
      root 'home#unregistered', as: :unauthenticated_root
    end
  end

  # para buscar el empleado
  get 'home/find_employee/:code', to: 'home#find_employee', as: :find_employee
  # para registrar hora
  post 'home/registry_time/:code', to: 'home#registry_time', as: :registry_time

  namespace :auth do
    resources :admins
    resources :employees
    resources :registries
    resources :working_days
    resources :schedules
    resources :departure_details
    resources :entry_details
  end

  namespace :worker do
    resources :employees, except: %i[index new create destroy] do
      member do
        get :records
      end
    end
  end

  root 'home#unregistered'
end
