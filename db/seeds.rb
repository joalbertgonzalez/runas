Schedule.create(description: '8:00 AM - 5:00 PM')
Schedule.create(description: '9:00 AM - 6:00 PM')

7.times do |i|
  WorkingDay.create(schedule: Schedule.find(1),
                    day: i,
                    entry_time: Time.zone.parse('8:00'),
                    departure_time: Time.zone.parse('17:00'))
end

7.times do |i|
  WorkingDay.create(schedule: Schedule.find(2),
                    day: i,
                    entry_time: Time.zone.parse('9:00'),
                    departure_time: Time.zone.parse('18:00'))
end

2.times do |i|
  user = User.new(email: "admin#{i + 1}@runa.com",
                  password: '12345678', # Devise.friendly_token[0,20]
                  password_confirmation: '12345678')

  user.profile = Admin.new(name: "admin#{i + 1}")
  user.save
end

10.times do |i|
  user = User.new(email: "Employee#{i + 1}@runa.com",
                  password: '12345678', # Devise.friendly_token[0,20]
                  password_confirmation: '12345678')

  user.profile = Employee.new(name: "Employee#{i + 1}",
                              schedule: Schedule.select(:id).find(1),
                              code: 1233 + i,
                              photo_url: 'https://avatars1.githubusercontent.com/u/13588707?s=400&u=c9610613c8807abf64d7be835bfacbe8ad898850&v=4')
  user.save
end

Registry.create(employee: Employee.find(1),
                date_time: Time.zone.parse(Time.now.to_s),
                category: :in)
Registry.create(employee: Employee.find(1),
                date_time: Time.zone.parse(Time.now.advance(hours: 6).to_s),
                category: :out)
