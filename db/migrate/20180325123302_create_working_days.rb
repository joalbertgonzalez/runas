class CreateWorkingDays < ActiveRecord::Migration[5.2]
  def change
    create_table :working_days do |t|
      t.references :schedule, foreign_key: true
      t.integer :day, null: false
      t.time :entry_time
      t.time :departure_time

      t.timestamps
    end
  end
end
