class AddGenderDniCivilStatusToEmployee < ActiveRecord::Migration[5.2]
  def change
    add_column :employees, :gender, :integer, default: 0
    add_column :employees, :dni, :integer
    add_column :employees, :civil_status, :integer, default: 0
  end
end
