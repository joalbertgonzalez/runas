class CreateDepartureDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :departure_details do |t|
      t.references :registry, foreign_key: true
      t.integer :minute_after_work, null: false, default: 0
      t.integer :minute_before_work, null: false, default: 0

      t.timestamps
    end
  end
end
