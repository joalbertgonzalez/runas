class AddPhotoUrlCodeDepartamentToEmployees < ActiveRecord::Migration[5.2]
  def change
    add_column :employees, :photo_url, :string
    add_column :employees, :code, :integer, null: false
    add_column :employees, :departament, :string
  end
end
