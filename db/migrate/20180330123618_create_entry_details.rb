class CreateEntryDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :entry_details do |t|
      t.references :registry, foreign_key: true
      t.integer :minute_delay, null: false, default: 0
      t.integer :minute_before, null: false, default: 0

      t.timestamps
    end
  end
end
