class AddWorkerHoursToDepartureDetails < ActiveRecord::Migration[5.2]
  def change
    add_column :departure_details, :worker_hours, :integer, null: false, default: 0
  end
end
