class CreateSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :schedules do |t|
      t.string :description
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
