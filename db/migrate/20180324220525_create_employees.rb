class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :last_name
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
