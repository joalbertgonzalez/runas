class CreateRegistries < ActiveRecord::Migration[5.2]
  def change
    create_table :registries do |t|
      t.references :employee, foreign_key: true
      t.timestamp :date_time, null: false
      t.integer :category
      t.string :ip

      t.timestamps
    end
  end
end
