# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_03_31_233753) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "name"
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departure_details", force: :cascade do |t|
    t.bigint "registry_id"
    t.integer "minute_after_work", default: 0, null: false
    t.integer "minute_before_work", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "worker_hours", default: 0, null: false
    t.index ["registry_id"], name: "index_departure_details_on_registry_id"
  end

  create_table "employees", force: :cascade do |t|
    t.string "name"
    t.string "last_name"
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "schedule_id"
    t.string "photo_url"
    t.integer "code", null: false
    t.string "departament"
    t.integer "gender", default: 0
    t.integer "dni"
    t.integer "civil_status", default: 0
    t.index ["schedule_id"], name: "index_employees_on_schedule_id"
  end

  create_table "entry_details", force: :cascade do |t|
    t.bigint "registry_id"
    t.integer "minute_delay", default: 0, null: false
    t.integer "minute_before", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["registry_id"], name: "index_entry_details_on_registry_id"
  end

  create_table "registries", force: :cascade do |t|
    t.bigint "employee_id"
    t.datetime "date_time", null: false
    t.integer "category"
    t.string "ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id"], name: "index_registries_on_employee_id"
  end

  create_table "schedules", force: :cascade do |t|
    t.string "description"
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "profile_id"
    t.string "profile_type"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "working_days", force: :cascade do |t|
    t.bigint "schedule_id"
    t.integer "day", null: false
    t.time "entry_time"
    t.time "departure_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["schedule_id"], name: "index_working_days_on_schedule_id"
  end

  add_foreign_key "departure_details", "registries"
  add_foreign_key "employees", "schedules"
  add_foreign_key "entry_details", "registries"
  add_foreign_key "registries", "employees"
  add_foreign_key "working_days", "schedules"
end
